<?php 
    require_once __DIR__.'/../util/Db.php';
    require_once __DIR__.'/../util/R.php';
    class CommentController{
        /**
         * 删除评论
         *
         * @return void
         */
        public function delCommentById(){
            session_start();
            try{
                $u_id = $_SESSION["islogin_token"];
                $c_id = $_GET["c_id"];
            }catch(Exception $e){
                echo R::error(403,"没有权限!");
                return ;
            }
            try{
                $sql = "delete from bbs_comment where id = :id and u_id = :u_id;";
                $con = Db::getinstance();
                $st = $con->prepare($sql);
                $st->bindParam(":id",$c_id);
                $st->bindParam(":u_id",$u_id);
                $st->execute();
                if($st->rowCount()>0){
                    echo R::error(200,"评论删除成功!");
                    return;
                }
            }catch(Exception $e){
            }
            echo R::error(403,"删除失败!");
            
            
        }
        /**
         * 进行评论
         *
         * @return void
         */
        public function addComment(){
            try{
                $content = $_GET["content"];
                $parent_id = (int)$_GET["parent_id"];
                session_start();
                $u_id = $_SESSION["islogin_token"];
                $a_id = $_GET["a_id"];
                $replay_id = (int)$_GET["replay_id"];
                $replay_name = $_GET["replay_name"];
            }catch(Exception $e){
                echo R::error(401,"进行评论失败");
                return ;
            }
            $sql = "INSERT INTO `quick_bbs`.`bbs_comment`(`content`, `parent_id`, `likes`, `sort`, `u_id`, `a_id`, `update_time`, `create_time`, `replay_id`, `replay_name`) VALUES (:content, :parent_id, 0, 0, :u_id, :a_id, now(), now(), :replay_id, :replay_name)";
            $con = Db::getinstance();
            $st = $con->prepare($sql);
            $st->bindParam(":content",$content);
            $st->bindParam(":parent_id",$parent_id);
            $st->bindParam(":u_id",$u_id);
            $st->bindParam(":a_id",$a_id);
            $st->bindParam(":replay_id",$replay_id);
            $st->bindParam(":replay_name",$replay_name);
            try{
                $st->execute();
            }catch(Exception $e){
                echo R::error(402,"评论失败！");
            }
            echo R::ok();
            

        }
        
        /**
         * 查询评论信息
         *
         * @return void
         */
        public function getCommentControllerByAid(){
            header('Content-Type:application/json;charset=utf-8');
            try{
                $a_id = $_GET["a_id"];
                $pageSize = $_GET["pageSize"];
                $index = $_GET["index"];
            }catch(Exception $e){
                echo R::error(401,"数据获取失败!");
                return;
            }
            /**
             * 查询评论总数
             */
            $sql = "select count(1) as total from bbs_comment as c inner join bbs_user as u on c.u_id = u.id where c.a_id = :a_id;";
            $con = Db::getinstance();
            $st = $con->prepare($sql);
            $st->bindParam(":a_id",$a_id);
            $st->execute();
            $res = $st->fetchAll();
            $total = $res[0]["total"];
            /**
             * 一级评论个数
             */
            $sql = "select count(1) as total from bbs_comment as c inner join bbs_user as u on c.u_id = u.id where c.a_id = :a_id and c.parent_id = 0";
            $con = Db::getinstance();
            $st = $con->prepare($sql);
            $st->bindParam(":a_id",$a_id);
            $st->execute();
            $res = $st->fetchAll();
            $totoal_one = $res[0]["total"];
            /**
             * 查询文章一级评论
             */
            $sql = "select u.avatar,u.username,c.content,c.create_time,c.likes,c.id,c.parent_id,c.replay_name from bbs_comment as c inner join bbs_user as u on c.u_id = u.id where c.a_id = :a_id and c.parent_id = 0 order by c.create_time asc limit :left,:right;";
            $con = Db::getinstance();
            $st = $con->prepare($sql);
            $left = $pageSize*($index-1);
            $st->bindParam(":a_id",$a_id);
            $st->bindParam(":left",$left,PDO::PARAM_INT);
            $st->bindParam(":right",$pageSize,PDO::PARAM_INT);
            $st->execute();
            $res = $st->fetchAll();
            $data = array();
            
            foreach($res as $item){
                //如果是0封装第一级
                if((int)$item["parent_id"] == 0){
                    $tmp = array(
                        "avatar" => $item["avatar"],
                        "username" => $item["username"],
                        "content" => $item["content"],
                        "create_time" => $item["create_time"],
                        "likes" => (int)$item["likes"],
                        "id" => (int)$item["id"],
                        "parent_id" => (int)$item["parent_id"],
                        "replay_name" => $item["replay_name"],
                        "parent" => array()
                    );
                    array_push($data,$tmp);
                }else{
                    
                    $parent_id = (int)$item["parent_id"];
                    $i = 0;
                    foreach($data as $item2){
                        if((int)$item2["id"] == $parent_id){
                            $tmp = array(
                                "avatar" => $item["avatar"],
                                "username" => $item["username"],
                                "content" => $item["content"],
                                "create_time" => $item["create_time"],
                                "likes" => (int)$item["likes"],
                                "id" => (int)$item["id"],
                                "parent_id" => (int)$item["parent_id"],
                                "replay_name" => $item["replay_name"],
                                "parent" => array()
                            );
                            array_push($data[$i]["parent"],$tmp);
                            $i++;

                        }
                    }
                }
            }
            $response = array(
                "totoal_one" =>$totoal_one,
                "total" => $total,
                "list" => $data
            );
            echo R::ok($response);

        }
    }
    $l = new CommentController();
    $router = $_GET["page"];
    if($router == "getCommentControllerByAid"){
        $l->getCommentControllerByAid();
    }else if($router == "addComment"){
        $l->addComment();
    }else if($router == "delCommentById"){
        $l->delCommentById();
    }else{
        header('HTTP/1.1 404 Not Found');exit('404');
    }

?>