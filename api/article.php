<?php 
 require_once __DIR__.'/../util/Db.php';
 require_once __DIR__.'/../util/R.php';
    /**
     * 发表文章类
     */
    class ArticleController{
        /**
         * 根据id删除文章（登录权限）
         *
         * @return void
         */
        public function delArticle(){
            session_start();
            if(!isset($_SESSION["islogin_token"]) || !isset($_SESSION["a_id"])){
                R::error(403,"权限不足!");
                return;
            }
            $u_id = $_SESSION["islogin_token"];
            $a_id = $_SESSION["a_id"];
            try{
                $sql = "delete from bbs_article where id = :id and u_id = :u_id";
                $con = Db::getinstance();
                $st = $con->prepare($sql);
                $st->bindParam(":id",$a_id);
                $st->bindParam(":u_id",$u_id);
                $st->execute();
                if($st->rowCount()>0){
                    echo R::error(200,"文章删除成功!");
                    return;
                }
            }catch(Exception $e){
            }
            echo R::error(403,"删除失败!");
        }
        /**
         * 根据id获取文章
         *
         * @return void
         */
        public function getArticleById(){
            if(isset($_GET["id"])){
                $id = $_GET["id"];
            }else{
                echo R::error(405,"缺少文章id值");
                return;
            }
            header('Content-Type:application/json;charset=utf-8');
            $sql = "select * from bbs_article where id = :id";
            $con = Db::getinstance();
            $st = $con->prepare($sql);
            $st->bindParam(":id",$id);
            try{
                $st->execute();
                $res = $st->fetchAll();
                if(count($res) == 0){
                    echo R::error(501,"没有查询到数据");
                    return;
                }
               
                
            }catch(Exception $e){
                echo R::error(409,"获取文章失败!");
                return;
            }
            session_start();
            
            if(isset($_SESSION["islogin_token"])){
                $res[0]["token_id"] = $_SESSION["islogin_token"];
            }
            echo R::ok($res[0]);
            
        }
        /**
         * 发布文章
         *
         * @return void
         */
        public function editArticle(){
            session_start();
            try{
                if(!isset($_SESSION["islogin_token"])){
                    echo R::error(403,"您还没有登录!");
                }
                $u_id = $_SESSION["islogin_token"];
                $a_id = $_POST["a_id"];
                $theme = $_POST["theme"];
                $introduction = $_POST["introduction"];
                $tag = $_POST["tag"];
                $content = $_POST["content"];
                $cover = $_POST["cover"];
            }catch (Exception $e) {
                echo R::error(401,"参数不匹配");
                return;
            }
            if($theme == "" || $content == "" || $cover == ""){
                echo R::error(402,"内容不能为空");
                return;
            }
            //写入数据
            $sql = "INSERT INTO `bbs_article`(`u_id`, `a_id`, `theme`, `introduction`, `tag`, `content`, `cover`, `sort`, `like`, `looks`, `comment_no`, `create_time`, `update_time`) VALUES (:u_id,:a_id,:theme,:introduction,:tag,:content,:cover,0,0,0,0,now(),now())";
            $con = Db::getinstance();
            $st = $con->prepare($sql);
            $st -> bindParam(":u_id",$u_id);
            $st -> bindParam(":a_id",$a_id);
            $st -> bindParam(":theme",$theme);
            $st -> bindParam(":introduction",$introduction);
            $st -> bindParam(":tag",$tag);
            $st -> bindParam(":content",$content);
            $st -> bindParam(":cover",$cover);
            try{
                $st->execute();
            }catch(Exception $e){
                echo R::error(408,"文章写入失败");
                return;
            }
            echo R::ok();
        }
        /**
         * 按照分类获取文章
         */
        public function getArticleByCate(){
            //获取个数
            header('Content-Type:application/json;charset=utf-8');
            if(isset($_GET["pageSize"])){
                $pageSize = $_GET["pageSize"];
            }
            //第几页
            if(isset($_GET["page_no"])){
                $page = $_GET["page_no"];
            }
            //文章分类id
            if(isset($_GET["a_id"])){
                $a_id = $_GET["a_id"];
            }
            //文章搜索分类
            if(isset($_GET["key"])){
                $key = $_GET["key"];
            }
            $left = $pageSize*($page-1);
            $right = $pageSize;
            //查询文章的总数
            $sql = 'select count(*) as total from bbs_article as a inner join bbs_user as u on a.u_id = u.id inner join bbs_categoriza as c on a.a_id = c.id where a.a_id = :a_id  and a.theme like CONCAT("%",:key,"%")';
            $con = Db::getinstance();
            $st = $con->prepare($sql);
            $st -> bindParam(":a_id",$a_id);
            $st -> bindParam(":key",$key);
            $st->execute();
            $res = $st->fetchAll();
            $total = number_format($res[0]["total"]);
            //读取文章数据
            $sql = 'select a.*,u.avatar,u.username,c.name,u.avatar from bbs_article as a inner join bbs_user as u on a.u_id = u.id inner join bbs_categoriza as c on a.a_id = c.id where a.a_id = :a_id  and a.theme like CONCAT("%",:key,"%") order by a.id desc,a.sort desc limit :left,:right';
    
            $st = $con->prepare($sql);
            $st -> bindParam(":a_id",$a_id);
            $st -> bindParam(":key",$key);
            $st -> bindParam(":left",$left,PDO::PARAM_INT);
            $st -> bindParam(":right",$right,PDO::PARAM_INT);
            $st->execute();
            $res = $st->fetchAll();
            $data = array(
                "total" => $total,
                "data" => $res
            );
            echo R::ok($data);
        }
    }
    $l = new ArticleController();
    $router = $_GET["page"];
    if($router == "getArticleByCate"){
        $l -> getArticleByCate();
    }else if($router == "editArticle"){
        $l -> editArticle();
    }else if($router == "getArticleById"){
        $l -> getArticleById();
    }
    else{
        header('HTTP/1.1 404 Not Found');exit('404');
    }

?>