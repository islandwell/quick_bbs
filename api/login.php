<?php 
include_once("../util/R.php");
include_once("../util/Db.php");
//kjE3
    //登录控制器
    
    class LoginController{
        function router(){
            $url = $_GET["page"];
            if($url == "login"){
                $this->login();
            }else if($url == "loginout"){
                $this->loginout();
            }
            else if($url == "drawcode"){
                $this->draw();
            }else{
                header('HTTP/1.1 404 Not Found');exit('404');
            }
        }
        function login(){
            header('Content-Type:application/json;charset=utf-8');
            $username = $_POST["username"];
            $password = $_POST["password"];
            $code = $_POST["code"];
            $lifeTime = 24 * 3600;
            session_set_cookie_params($lifeTime);
            session_start();
            $right_code = $_SESSION["code"];
            // 判断
            if($password == "" || $username == ""){
                echo R::error(201,"用户名或者密码不能为空!");
                exit(0);
            }
            // 判断验证码

            if(strtoupper($right_code) != strtoupper($code)){
                echo R::error(201,"验证码不正确!");
                exit(0);
            }
            //开始登录逻辑 查询用户名密码
            $con = Db::getinstance();
            $st = $con->prepare("SELECT * FROM `bbs_user` where password = :password and username = :username");
            $st->bindParam(":password",$password);
            $st->bindParam(":username",$username);
            $st->execute();
            $res = $st->fetchAll();
            if(count($res) == 0){
                echo R::error(201,"用户名或者密码不正确!");
                exit(0);
            }
            //开启一个会session会话
            $_SESSION["username"] = $username;
            $user = $res[0];
            $_SESSION["islogin_token"] = $user["id"];
            $data = array(
                "username" => $user['username'],
                "avatar" => $user['avatar'],
                "profile" => $user['profile']
            );
            //使得验证码的session过期
            unset($_SESSION['code']);
            echo R::ok($data);
        }
        //画验证码
        function draw(){
            include_once("../util/VercodeUtil.php");
            $image = imagecreate(200, 100);
            imagecolorallocate($image, 0, 0, 0);
            for ($i=0; $i <= 9; $i++) {
                // 绘制随机的干扰线条
                imageline($image, rand(0, 200), rand(0, 100), rand(0, 200), rand(0, 100), rand_color($image));
            }
            for ($i=0; $i <= 100; $i++) {
                // 绘制随机的干扰点
                imagesetpixel($image, rand(0, 200), rand(0, 100), rand_color($image));
            }
            $length = 4;//验证码长度
            $str = rand_str($length);//获取验证码
            include_once("../config/config.php");
            $font =config::$source["font"];
            for ($i=0; $i < $length; $i++) {
                // 逐个绘制验证码中的字符
                imagettftext($image, rand(20, 38), rand(0, 60), $i*50+25, rand(30,70), rand_color($image), $font, $str[$i]);
            }
            session_start();
            header('Content-type:image/jpeg');
            $_SESSION["code"] = $str;
            imagejpeg($image);
            imagedestroy($image);
        }
        /**
         * 退出登录
         *
         * @return void
         */
        function loginout(){
            //清空所有的session
            session_start();
            unset($_SESSION['code']);
            unset($_SESSION['islogin_token']);
            echo R::ok(null);
        }
        
    }
    
    
    $l = new LoginController();
    $l->router();
    
?>