<?php 
    include "../util/R.php";
    include "../util/Db.php";
    class RegisterController{
        public function register(){
            header('Content-Type:application/json;charset=utf-8');
            $username = $_POST["username"];
            $password = $_POST["password"];
            $restPassword = $_POST["restPassword"];
            $avatar = $_POST["avatar"];
            $profile = $_POST["profile"];
            session_start();
            $code = $_POST["code"];
            $res = $_SESSION["reg_code"];
            if(strtoupper($code) != strtoupper($res)){
                echo R::error(402,"验证码不正确");
                exit(0);
            }
            if(!isset($username)||!isset($password)||!isset($restPassword)){
                echo R::error(401,"输入的信息不完整!",);
                exit(0);
            }
            if($password != $restPassword){
                echo R::error(401,"密码不匹配!");
                exit(0);
            }
            //用户名不能重复
            $sql = "select * from bbs_user where username = :username";
            $con = Db::getinstance();
            $st = $con->prepare($sql);
            $st->bindParam(":username",$username);
            $st->execute();
            $res = $st->fetchAll();
            if(count($res) > 0){
                echo R::error(403,"用户名已经被注册了，注册失败!");
                exit();
            }
            //判断完成开始注册业务逻辑
            $sql = "insert into bbs_user(username,avatar,profile,password,create_time,update_time) VALUES(:username,:avatar,:profile,:password,now(),now())";
            //获取sql链接
            $con = Db::getinstance();
            $st = $con->prepare($sql);
            $st->bindParam(":username",$username);
            $st->bindParam(":avatar",$avatar);
            $st->bindParam(":profile",$profile);
            $st->bindParam(":password",$password);
            $status = $st->execute();
            if(!$status){
                echo R::error(403,"用户注册失败!");
                exit();
            }
            //注册成功验证码失效
            unset($_SESSION["reg_code"]);
            echo R::ok("注册用户成功!");
        }
        function draw(){
            include_once("../util/VercodeUtil.php");
            $image = imagecreate(200, 100);
            imagecolorallocate($image, 0, 0, 0);
            for ($i=0; $i <= 9; $i++) {
                // 绘制随机的干扰线条
                imageline($image, rand(0, 200), rand(0, 100), rand(0, 200), rand(0, 100), rand_color($image));
            }
            for ($i=0; $i <= 100; $i++) {
                // 绘制随机的干扰点
                imagesetpixel($image, rand(0, 200), rand(0, 100), rand_color($image));
            }
            $length = 4;//验证码长度
            $str = rand_str($length);//获取验证码
            include_once("../config/config.php");
            $font =config::$source["font"];
            for ($i=0; $i < $length; $i++) {
                // 逐个绘制验证码中的字符
                imagettftext($image, rand(20, 38), rand(0, 60), $i*50+25, rand(30,70), rand_color($image), $font, $str[$i]);
            }
            session_start();
            header('Content-type:image/jpeg');
            $_SESSION["reg_code"] = $str;
            imagejpeg($image);
            imagedestroy($image);
        }
    }
    $l = new RegisterController();
    $page = $_GET["page"];
    if($page == "vercode"){
        $l->draw();
    }else if($page == "register"){
        $l->register();
    }else{
        header('HTTP/1.1 404 Not Found');exit('404');
    }
?>