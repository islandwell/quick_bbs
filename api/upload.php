<?php
use OSS\OssClient;
use OSS\Core\OssException;
require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__.'/../util/R.php';
/**
 * 策略模式 wang上传图片和普通上传图片都是上传图片
 * 都是上传图片的效果，得到相同的结果。
 */
interface UploadBaseAgent{
    public function uploadFile();
    
}
/**
 * 正常上传文件
 */
class CommonUploadFile implements UploadBaseAgent{
    public function LocaluploadFile(){
        if(isset($_FILES['file'])){
            $errors= array();
            $file_name = $_FILES['file']['name'];
            $file_size = $_FILES['file']['size'];
            $file_tmp = $_FILES['file']['tmp_name'];
            $file_type = $_FILES['file']['type'];
            $name_arr = explode('.',$_FILES['file']['name']);
            $file_ext=strtolower(end($name_arr));
            $file_data = date("Ymd");
            move_uploaded_file($file_tmp,__DIR__."/../resource/$file_data/$file_name");
            return $file_name;
        }
        return null;
    }

    public function uploadFile(){
        header('Content-Type:application/json;charset=utf-8');
        if(isset($_GET['osstoken'])){
            $str = $_GET['osstoken'];
            if($str != "as-ajfiejafjjfieiiajfeifafeefffffi"){
                echo R::error(403,"权限不足");
                return;
            }
        }else{
            echo R::error(403,"权限不足");
            return;
        }
        // 阿里云主账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM账号进行API访问或日常运维，请登录RAM控制台创建RAM账号。
        $accessKeyId = "LTAI5tGqENhYGGYJdQH8EUPj";
        $accessKeySecret = "jffSVGULQPLWi09DLszd1silgATadm";
        $securityToken = "abacdsesfj$%Iijfiaoa:ajfeifai?oifei";
        // yourEndpoint填写Bucket所在地域对应的Endpoint。以华东1（杭州）为例，Endpoint填写为https://oss-cn-hangzhou.aliyuncs.com。
        $endpoint = "http://oss-cn-beijing.aliyuncs.com";
        $endpointname = "oss-cn-beijing.aliyuncs.com";
        // 填写Bucket名称。
        $bucket= "gulimaill-hello-test";
        #//拼接的路径名
        // /quick_bbs/image/日期/随机文件名字
        $file_data = date("Ymd");
        // Endpoint以杭州为例，其它Region请按实际情况填写。
        $bucket= "gulimaill-hello-test";
        // 设置文件名称。

        try{
            $path = __DIR__."/../resource/$file_data";
        if (!is_dir($path)){ 
            mkdir($path,0777,true);
        }
        
            $filename = $filePath = $this->LocaluploadFile();
        }catch(Exception $e){
            echo "上传失败";
            return;
        }
        
        $object = "quick_bbs/image/$file_data/$filename";
        $filePath = "$path/$filename";

        try{
            $ossClient = new OssClient($accessKeyId, $accessKeySecret, $endpoint);
            $ossClient->uploadFile($bucket, $object, $filePath);
        } catch(OssException $e) {
            printf(__FUNCTION__ . ": FAILED\n");
            printf($e->getMessage() . "\n");
            return;
        }
        //成功返回url
        $url = "https://$bucket.$endpointname/$object";
        echo R::ok($url);
    }


}
/**
 * 王上传文件
 */
class WangUploadFile implements UploadBaseAgent{
    public function LocaluploadFile(){
        foreach($_FILES as $item){
            $file = $item;
        }
        try{
            $file_name = $file['name'];
            $file_size = $file['size'];
            $file_tmp = $file['tmp_name'];
            $file_type = $file['type'];
            $name_arr = explode('.',$file['name']);
            $file_ext=strtolower(end($name_arr));
            $file_data = date("Ymd");
            move_uploaded_file($file_tmp,__DIR__."/../resource/$file_data/$file_name");
        }catch(Exception $e){
            return null;
        }
        
        return $file_name;
    }
    public function uploadFile(){
       
    header('Content-Type:application/json;charset=utf-8');
    if(isset($_GET['osstoken'])){
        $str = $_GET['osstoken'];
        if($str != "as-dffdsasfwefefasdfdafafff"){
            echo R::error(403,"权限不足");
            return;
        }
    }else{
        echo R::error(403,"权限不足");
        return;
    }
    // 阿里云主账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM账号进行API访问或日常运维，请登录RAM控制台创建RAM账号。
    $accessKeyId = "LTAI5tGqENhYGGYJdQH8EUPj";
    $accessKeySecret = "jffSVGULQPLWi09DLszd1silgATadm";
    $securityToken = "abacdsesfj$%Iijfiaoa:ajfeifai?oifei";
    // yourEndpoint填写Bucket所在地域对应的Endpoint。以华东1（杭州）为例，Endpoint填写为https://oss-cn-hangzhou.aliyuncs.com。
    $endpoint = "http://oss-cn-beijing.aliyuncs.com";
    $endpointname = "oss-cn-beijing.aliyuncs.com";
    // 填写Bucket名称。
    $bucket= "gulimaill-hello-test";
    #//拼接的路径名
    // /quick_bbs/image/日期/随机文件名字
    $file_data = date("Ymd");
    // Endpoint以杭州为例，其它Region请按实际情况填写。
    $bucket= "gulimaill-hello-test";
    // 设置文件名称。
    try{
        $path = __DIR__."/../resource/$file_data";
    if (!is_dir($path)){ 
        mkdir($path,0777,true);
    }
        $filename = $filePath = $this->LocaluploadFile();
    }catch(Exception $e){
        echo "上传失败";
        return;
    }
    $object = "quick_bbs/image/$file_data/$filename";


    $filePath = "$path/$filename";

    try{
        $ossClient = new OssClient($accessKeyId, $accessKeySecret, $endpoint);

        $ossClient->uploadFile($bucket, $object, $filePath);
    } catch(OssException $e) {
        printf(__FUNCTION__ . ": FAILED\n");
        printf($e->getMessage() . "\n");
        return;
    }
    //成功返回url
    $url = "https://$bucket.$endpointname/$object";
    $map = array(
        "errno" => 0,
        "data" => [0 => [
            "url" => "$url",
            "alt" => "",
            "href" => "#"
            ]
        ]
        );
    echo json_encode($map);
    }
}

class Browser{
    public function call($object){
        $object->uploadFile();
    }
}
$bro = new Browser();
$router = $_GET["page"];
if($router == "commonUpload"){
    $bro->call(new CommonUploadFile());
}else if($router == "wangEditUpload"){
    $bro->call(new WangUploadFile());
}
?>