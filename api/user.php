<?php 
/**
 * 封装了工厂模式
 */
    require_once __DIR__.'/../util/R.php';
    require_once __DIR__.'/../util/Db.php';

      /**
       * 工厂操作类
       */
    interface operation{
        public function getResult();
    }
    /**
     * 更新信息个人信息
     */
    class UpdateUserOperation implements operation{
        public function getResult(){
            header('Content-Type:application/json;charset=utf-8');
           
            $profile = "";
            if(isset($_GET["avatar"])){
                $avatar = $_GET["avatar"];
            }else{
                echo R::error(401,"必须要有一个头像");
                return;
            }
            if(isset($_GET["profile"])){
                $profile = $_GET["profile"];
            }
            session_start();
            if(isset($_SESSION["islogin_token"])){
                $id = $_SESSION["islogin_token"];
            }else{
                echo R::error(401,"无权限");
                return;
            }
            $sql = "UPDATE `quick_bbs`.`bbs_user` SET  `avatar` = :avatar, `profile` = :profile,`update_time` = now() WHERE `id` = :id";
            $con = Db::getinstance();
            $st = $con->prepare($sql);
            $st->bindParam(":avatar",$avatar);
            $st->bindParam(":profile",$profile);
            $st->bindParam(":id",$id);
            $st->execute();
            $res = $st->fetchAll();
            echo R::ok($res);
        }
    }
     /**
      * 获取个人信息
      */
      class GetUserOperation implements operation{
        public function getResult(){
            header('Content-Type:application/json;charset=utf-8');
            session_start();
            $id = $_SESSION["islogin_token"];
            $username = $_GET["username"];
            $sql = "select * from bbs_user where id = :id and username = :username";

            $con = Db::getinstance();
            $st = $con->prepare($sql);
            $st->bindParam(":id",$id);
            $st->bindParam(":username",$username);
            $st->execute();
            $res = $st->fetchAll();
            echo R::ok($res[0]);
        }
    }
    /**
     * 工厂类
     */
    class OperationFactory{
        //创建保存示例的静态成员变量
        private static $obj;
        //创建实例的静态方法
        public static function CreateOperation($type){
            switch($type){
                case "update":
                    self::$obj = new UpdateUserOperation();
                    break;
                case "get":
                    self::$obj = new GetUserOperation();
                    break;
            }
            return self::$obj;
        }
    }
    
    $page = $_GET["page"];
    $obj = OperationFactory::CreateOperation($page);
    $obj->getResult();

?>