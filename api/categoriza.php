<?php 
require_once __DIR__.'/../util/Db.php';
require_once __DIR__.'/../util/R.php';
class CategorizaController{
    public function getCateGorizaList(){
        header('Content-Type:application/json;charset=utf-8');
        $sql = "select * from bbs_categoriza";
        $con = Db::getinstance();
        $st = $con->prepare($sql);
        $st->execute();
        $res = $st->fetchAll();
        $data = array();
        foreach($res as $item){
            $obj = array(
                "a_id"=>$item["id"],
                "name"=>$item["name"],
                "sort"=>$item["sort"],
                "create_time"=>$item["create_time"],
                "update_time"=>$item["update_time"],
                "logo"=>$item["logo"]
            );
            array_push($data,$obj);
        }
        echo R::ok($data);
    }
}
    $l = new CategorizaController();
    $page = $_GET["page"];
    if($page == "getCateGorizaList"){
        $l->getCateGorizaList();
    }else{
        header('HTTP/1.1 404 Not Found');exit('404');
    }

?>