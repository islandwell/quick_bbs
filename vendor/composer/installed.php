<?php return array(
    'root' => array(
        'pretty_version' => '1.0.0+no-version-set',
        'version' => '1.0.0.0',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => NULL,
        'name' => '__root__',
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => '1.0.0+no-version-set',
            'version' => '1.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => NULL,
            'dev_requirement' => false,
        ),
        'aliyuncs/oss-sdk-php' => array(
            'pretty_version' => 'v2.4.2',
            'version' => '2.4.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../aliyuncs/oss-sdk-php',
            'aliases' => array(),
            'reference' => '0c9d902c33847c07efc66c4cdf823deaea8fc2b6',
            'dev_requirement' => false,
        ),
    ),
);
