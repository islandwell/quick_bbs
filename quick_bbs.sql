/*
 Navicat Premium Data Transfer

 Source Server         : 朱同学的本地电脑
 Source Server Type    : MySQL
 Source Server Version : 100417
 Source Host           : localhost:3306
 Source Schema         : quick_bbs

 Target Server Type    : MySQL
 Target Server Version : 100417
 File Encoding         : 65001

 Date: 20/06/2021 20:09:58
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for bbs_article
-- ----------------------------
DROP TABLE IF EXISTS `bbs_article`;
CREATE TABLE `bbs_article`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '文章id',
  `u_id` int(11) NULL DEFAULT NULL COMMENT '发表的用户id',
  `a_id` int(11) NULL DEFAULT NULL COMMENT '分类id',
  `theme` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '主题',
  `introduction` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文章简介',
  `tag` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标签',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '文章内容',
  `cover` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文章封面',
  `sort` int(11) NOT NULL DEFAULT 0 COMMENT '排序',
  `like` int(11) NULL DEFAULT 0 COMMENT '点赞数',
  `looks` int(11) NULL DEFAULT 0 COMMENT '浏览数',
  `comment_no` int(11) NOT NULL DEFAULT 0 COMMENT '评论数',
  `create_time` datetime(0) NOT NULL COMMENT '发表时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 47 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bbs_article
-- ----------------------------
INSERT INTO `bbs_article` VALUES (34, 5, 4, '印度一医院模拟演习致死22名病人', '据俄罗斯卫星通讯社8日报道，近日一段视频在社交媒体上疯传，显示印度阿格拉地区一家私人医院的老板承认曾切断危重病人的氧气供应，这导致北方邦政府官员为此展开调查。', '印度疫情', '<div class=\"article\" id=\"article\">\r\n				<p>　　原标题：印度一医院模拟演习致死22名病人？！</p>\r\n<p>　　据俄罗斯卫星通讯社8日报道，近日一段视频在社交媒体上疯传，显示印度阿格拉地区一家私人医院的老板承认曾切断危重病人的氧气供应，这导致北方邦政府官员为此展开调查。</p>\r\n<p>　　阿林杰·贾恩是帕拉斯医院的老板，他声称该医院在4月26日的一次“模拟演习”中，将危重病人的氧气供应切断了5分钟。</p>\r\n<p>　　“我决定尝试一些类似模拟演习的东西，并请工作人员识别哪些人的氧气供应可以被切断。这样，我们就能知道（在没有氧气的情况下）谁会死，谁会活下来。模拟演习是在早上7点进行的，没有人知道这件事。”贾恩在视频中说。</p>\r\n<p>　　“氧气被切断后，22名患者开始呼吸困难，身体开始变蓝，我们知道他们活不了多久了。”他补充说，“在重症监护室的其他74名幸存者被告知，要自己带氧气瓶。”</p>\r\n<p cms-style=\"font-L\">　　当俄罗斯卫星通讯社联系阿格拉当地治安官时，他澄清说只有7名病人在4月26日和27日死亡。</p>\r\n<div class=\"img_wrapper\"><img id=\"0\" style=\"max-width: 640px;\" src=\"//n.sinaimg.cn/sinakd202169s/303/w641h462/20210609/e15f-krhvrxt2935987.jpg\" alt=\"\"><span class=\"img_descr\"></span></div>\r\n<p cms-style=\"font-L\">　　官员表示：<font cms-style=\"font-L color330\"><font cms-style=\"font-L strong-Bold color330\">“我们正在调查此事，那天并没有22个病人死亡。地方治安官正在调查此事，我现在什么都不能说，我们只有在调查结束后才会做出决定，我们会对罪犯采取严厉行动。”</font></font></p>\r\n<p cms-style=\"font-L\">　　与此同时，印度主要反对党国大党的两名政客拉胡尔·甘地（Rahul Gandhi）和普里扬卡·甘地·瓦德拉（Priyanka Gandhi Vadra）周二抨击了印度人民党领导的北方邦政府。</p>\r\n<p cms-style=\"font-L\">　　甘地还在推特上分享了这段丑闻视频，并写道：“在人民党统治下，氧气和人力都严重短缺。应该立即采取行动，打击所有应该为这一危险罪行负责的人。在这个悲痛的时刻，我向死者的家人表示哀悼。”</p>\r\n<p cms-style=\"font-L\">　　印度新冠疫情肆虐之际，氧气等医疗资源陷入严重短缺，不少新冠患者因此去世。据印媒此前报道，印度南部安得拉邦蒂鲁伯蒂（Tirupati）某医院重症监护病房内的11名新冠患者因氧气供应中断而去世。报道称，尽管病人家属称氧气供应中断了约45分钟，但相关负责人表示，“重新装填氧气瓶有5分钟的延迟，导致压力下降”，最终造成病人死亡。</p>\r\n<div class=\"img_wrapper\"><img id=\"1\" style=\"max-width: 640px;\" src=\"//n.sinaimg.cn/sinakd202169s/268/w641h427/20210609/1294-krhvrxt2935986.jpg\" alt=\"\"><span class=\"img_descr\"></span></div>\r\n<p cms-style=\"font-L\">　　海得拉巴市一家医院此前也因氧气供应中断而导致7名患者死亡，不过该医院否认这一说法。</p>\r\n<p cms-style=\"font-L\">　　面对严峻的疫情形势，印度的医疗系统已不堪重负，医疗物资严重短缺，尤其是医用氧气。为应对缺氧问题，印度政府开通了“氧气专列”，并建造新的氧气生产厂。然而一些印度媒体和民众认为，政府的行动太晚、太慢了。</p>\r\n<p>　　<font cms-style=\"font-L align-Center\"><font cms-style=\"font-L align-Center\"><font cms-style=\"font-L align-Center\"> <!--article_adlist[<p cms-style=\"font-L align-Center color0\"><font cms-style=\"font-L align-Center color0\">了解《环球时报》的三观</font></p><p cms-style=\"font-L\">　　请长按下方二维码关注我们or回到文章顶部，点击环球时报 （微信公众号ID：hqsbwx）</p>]article_adlist--></font></font></font></p>\r\n<div class=\"img_wrapper\"><font cms-style=\"font-L align-Center\"><!--article_adlist[<img cms-width=\"197.078\" style=\"max-width:640px\" id=\"3\" src=\"//n.sinaimg.cn/sinakd202169s/560/w1080h1080/20210609/9048-krhvrxt0601776.jpg\" img-code=\"2048\" img-size=\"1080,1080\" ocr-text=\"白吃\\n西神旦\\n个大\\n丸殊的\\n能心动还\" qr-area=\"0.735\"/>]article_adlist--><span class=\"img_descr\"></span></font></div>\r\n								\r\n				  \r\n				\r\n				\r\n<div id=\"ad_44086\" class=\"otherContent_01\" style=\"display: block; margin: 10px 20px 10px 0px; float: left; overflow: hidden; clear: both; padding: 4px; width: 336px; height: 280px;\"><ins class=\"sinaads sinaads-done\" id=\"Sinads49447\" data-ad-pdps=\"PDPS000000044089\" data-ad-status=\"done\" style=\"display: block; overflow: hidden; text-decoration: none;\"><ins style=\"text-decoration: none; margin: 0px auto; width: 336px; display: block; position: relative; overflow: hidden; height: 280px;\"><iframe adtypeturning=\"false\" width=\"300px\" height=\"250px\" frameborder=\"0\" marginwidth=\"0\" marginheight=\"0\" vspace=\"0\" hspace=\"0\" allowtransparency=\"true\" scrolling=\"no\" sandbox=\"allow-popups allow-same-origin allow-scripts allow-top-navigation-by-user-activation\" src=\"javascript:\'<html><body style=background:transparent;></body></html>\'\" id=\"sinaadtk_sandbox_id_10\" style=\"float: left; width: 336px; height: 280px;\" name=\"sinaadtk_sandbox_id_10\"></iframe></ins></ins></div><p class=\"show_author\">责任编辑：刘光博 </p><div style=\"font-size: 0px; height: 0px; clear: both;\"></div>\r\n				\r\n			<div id=\"cpro_u6395355\" style=\"margin-top: 30px; text-align: center;\"><iframe id=\"iframeu6395355_0\" name=\"iframeu6395355_0\" src=\"https://pos.baidu.com/mcrm?conwid=640&amp;conhei=240&amp;rdid=6395355&amp;dc=3&amp;exps=110261,110254,110011&amp;psi=82caff8e08df2ad214ae2917534ea966&amp;di=u6395355&amp;dri=0&amp;dis=0&amp;dai=21&amp;ps=2943x331&amp;enu=encoding&amp;ant=0&amp;aa=1&amp;dcb=___adblockplus_&amp;dtm=HTML_POST&amp;dvi=0.0&amp;dci=-1&amp;dpt=none&amp;tsr=0&amp;tpr=1623253275826&amp;ti=%E5%8D%B0%E5%BA%A6%E4%B8%80%E5%8C%BB%E9%99%A2%E6%A8%A1%E6%8B%9F%E6%BC%94%E4%B9%A0%E8%87%B4%E6%AD%BB22%E5%90%8D%E7%97%85%E4%BA%BA%EF%BC%9F%EF%BC%81%7C%E5%8D%B0%E5%BA%A6%7C%E7%94%98%E5%9C%B0_%E6%96%B0%E6%B5%AA%E6%96%B0%E9%97%BB&amp;ari=2&amp;ver=0608&amp;dbv=2&amp;drs=3&amp;pcs=1903x969&amp;pss=1903x5822&amp;cfv=0&amp;cpl=3&amp;chi=1&amp;cce=true&amp;cec=UTF-8&amp;tlm=1623253276&amp;prot=2&amp;rw=969&amp;ltu=https%3A%2F%2Fnews.sina.com.cn%2Fw%2F2021-06-09%2Fdoc-ikqciyzi8710928.shtml&amp;ltr=https%3A%2F%2Fnews.sina.com.cn%2Fworld%2F&amp;ecd=1&amp;uc=1920x1040&amp;pis=-1x-1&amp;sr=1920x1080&amp;tcn=1623253277&amp;qn=8ba21b17b9e3eaeb&amp;tt=1623253276639.12.12.12\" width=\"640\" height=\"240\" scrolling=\"no\" frameborder=\"0\"></iframe></div><script src=\"//cpro.baidustatic.com/cpro/ui/c.js\"></script></div>', 'https://gulimaill-hello-test.oss-cn-beijing.aliyuncs.com/quick_bbs/image/20210609/e15f-krhvrxt2935987.jpg', 0, 0, 0, 0, '2021-06-09 23:44:07', '2021-06-09 23:44:07');
INSERT INTO `bbs_article` VALUES (35, 5, 1, '印度疫情图片', '疫情图片', '22', '<p><img src=\"https://gulimaill-hello-test.oss-cn-beijing.aliyuncs.com/quick_bbs/image/20210609/e15f-krhvrxt2935987.jpg\" data-href=\"%23\" style=\"max-width:100%;\" contenteditable=\"false\"/></p>', 'https://gulimaill-hello-test.oss-cn-beijing.aliyuncs.com/quick_bbs/image/20210609/e15f-krhvrxt2935987.jpg', 0, 0, 0, 0, '2021-06-10 00:17:32', '2021-06-10 00:17:32');
INSERT INTO `bbs_article` VALUES (37, 5, 1, '一起来游泳吧', '一起来游泳吧', '游泳', '<p><img src=\"https://gulimaill-hello-test.oss-cn-beijing.aliyuncs.com/quick_bbs/image/20210610/0ac0e8da6ff16868edcdfd30a7aaf00d8a7895f4.jpg\" data-href=\"%23\" style=\"max-width:100%;\" contenteditable=\"false\"/><br/><img src=\"https://gulimaill-hello-test.oss-cn-beijing.aliyuncs.com/quick_bbs/image/20210610/04a332d647418cc25eba69712db139a19d345248.jpg\" data-href=\"%23\" style=\"max-width:100%;\" contenteditable=\"false\"/><br/><br/></p><p><img src=\"https://gulimaill-hello-test.oss-cn-beijing.aliyuncs.com/quick_bbs/image/20210610/a319fe0ccb4822f2530683c593ae541989b87c67.jpg\" data-href=\"%23\" style=\"max-width:100%;\" contenteditable=\"false\"/></p><p><br/></p><p><img src=\"https://gulimaill-hello-test.oss-cn-beijing.aliyuncs.com/quick_bbs/image/20210610/d1063a612c95a82863c5a7581012f802d0130820.jpg\" data-href=\"%23\" style=\"max-width:100%;\" contenteditable=\"false\"/></p>', 'https://gulimaill-hello-test.oss-cn-beijing.aliyuncs.com/quick_bbs/image/20210610/0ac0e8da6ff16868edcdfd30a7aaf00d8a7895f4.jpg', 0, 0, 0, 0, '2021-06-10 10:38:11', '2021-06-10 10:38:11');
INSERT INTO `bbs_article` VALUES (38, 1, 1, '不良jk', '到底是真不良还是装的？', '黑丝', '<p><img src=\"https://gulimaill-hello-test.oss-cn-beijing.aliyuncs.com/quick_bbs/image/20210610/bb7ae8d236091766990be48ea12d4707b352a0b9.png\" data-href=\"%23\" style=\"max-width:100%;\" contenteditable=\"false\"/></p>', 'https://gulimaill-hello-test.oss-cn-beijing.aliyuncs.com/quick_bbs/image/20210610/bb7ae8d236091766990be48ea12d4707b352a0b9.png', 0, 0, 0, 0, '2021-06-10 10:42:45', '2021-06-10 10:42:45');
INSERT INTO `bbs_article` VALUES (39, 5, 1, '【第十七期·嘉然gif楼】R1.8m,嘉丽然梦露的绝密镜头大放送!', '嘉丽然·梦露，一个熟悉的名字，熟悉的形像。以她那招牌式的笑容，充满诱惑力的肢体语言，塑造了一个又一个性感形象。她动人的表演风格，成为嘉心糖心中永远的性感符号和流行文化的代表性人物。很多人成长过程中都或多或少见过她那些照片，并记忆深刻：那张穿着粉色裙子在房间里，裙子吹得张开了喇叭的gif图；那张面对镜头两眼微阖、双唇微翘的；在沙发上小抽一口烟的。这些图片不再是图片，而成了一种特殊情感的传达，一种纯真性感的定格。', '动漫', '<h1 style=\"text-align:center;\">【嘉然GIF索引楼·长期更新】https://www.douban.com/group/topic/225751060/<br/></h1><h1 style=\"text-align:center;\">找不到过往图楼的小伙伴可以点进索引楼or关注我哦！啾咪！</h1><p style=\"text-align:left;\">嘉丽然·梦露，一个熟悉的名字，熟悉的形像。以她那招牌式的笑容，充满诱惑力的肢体语言，塑造了一个又一个性感形象。她动人的表演风格，成为嘉心糖心中永远的性感符号和流行文化的代表性人物。很多人成长过程中都或多或少见过她那些照片，并记忆深刻：那张穿着粉色裙子在房间里，裙子吹得张开了喇叭的gif图；那张面对镜头两眼微阖、双唇微翘的；在沙发上小抽一口烟的。这些图片不再是图片，而成了一种特殊情感的传达，一种纯真性感的定格。<br/><br/>今日，请跟随记者的脚步，共同鉴赏一组嘉丽然梦露不为人知的绝密镜头。<br/></p><p style=\"text-align:center;\"><img src=\"https://gulimaill-hello-test.oss-cn-beijing.aliyuncs.com/quick_bbs/image/20210611/00c48d6336eb73b193817ee385e171a11b0ce85f.gif\" data-href=\"%23\" style=\"max-width:100%;\" contenteditable=\"false\"/><br/></p><p style=\"text-align:center;\"><img src=\"https://gulimaill-hello-test.oss-cn-beijing.aliyuncs.com/quick_bbs/image/20210611/283f2ad98fdcb543244778b5a600bf9dfcc8ed5e.gif\" data-href=\"%23\" style=\"max-width:100%;\" contenteditable=\"false\"/><br/><br/></p>', 'https://gulimaill-hello-test.oss-cn-beijing.aliyuncs.com/quick_bbs/image/20210611/00c48d6336eb73b193817ee385e171a11b0ce85f.gif', 0, 0, 0, 0, '2021-06-12 01:18:51', '2021-06-12 01:18:51');
INSERT INTO `bbs_article` VALUES (40, 5, 2, '【美图搬运】第13期', '【美图搬运】第13期', '动漫杂谈', '<h1 style=\"text-align:center;\">【美图搬运】第13期</h1><p style=\"text-align:center;\"><br/></p><p style=\"text-align:center;\"><img src=\"https://gulimaill-hello-test.oss-cn-beijing.aliyuncs.com/quick_bbs/image/20210611/a7m02-676pl.png\" data-href=\"%23\" style=\"max-width:100%;\" contenteditable=\"false\"/><br/><img src=\"https://gulimaill-hello-test.oss-cn-beijing.aliyuncs.com/quick_bbs/image/20210611/a930t-iz4jy.png\" data-href=\"%23\" style=\"max-width:100%;\" contenteditable=\"false\"/></p><p style=\"text-align:center;\"><img src=\"https://gulimaill-hello-test.oss-cn-beijing.aliyuncs.com/quick_bbs/image/20210611/aer9q-i6x5b.png\" data-href=\"%23\" style=\"max-width:100%;\" contenteditable=\"false\"/><br/><img src=\"https://gulimaill-hello-test.oss-cn-beijing.aliyuncs.com/quick_bbs/image/20210611/asguh-7pt7e.png\" data-href=\"%23\" style=\"max-width:100%;\" contenteditable=\"false\"/></p>', 'https://gulimaill-hello-test.oss-cn-beijing.aliyuncs.com/quick_bbs/image/20210611/dab587b84c5955cfe25d795e2abfeeb8a2f69011.jpg@628w_901h_progressive.webp', 0, 0, 0, 0, '2021-06-12 01:25:54', '2021-06-12 01:25:54');
INSERT INTO `bbs_article` VALUES (41, 5, 1, '【新人Vup出道预告】初次见面！先一睹taffy黑影之下的超美丽身姿吧？', '【新人Vup出道预告】初次见面！先一睹taffy黑影之下的超美丽身姿吧？', '推荐', '<p style=\"text-align:center;\"><b><font face=\"微软雅黑\">关于传播分：<br/><br/>1-4分：粉丝向内容<br/><br/>4-7分：优秀内容<br/><br/>7-10分：爆款内容<br/><br/>每日虚拟UP主（VUP、VTuber）最佳动画视频投稿TOP10<br/><br/><br/>TOP1&nbsp;嘉然今天吃什么（三连+37172，传播 7.5分）<br/><br/>00:13<br/>【嘉然】终于。。。看来魔法少女的身份要瞒不住啦！！！(∩^o^)⊃━☆゜.*<br/> 40.1万  7500<br/>视频<br/>嘉然今天吃什么<br/><br/>TOP2&nbsp;永雏塔菲（三连+15507，传播 4.6分）<br/><br/>15:31<br/>【新人Vup出道预告】初次见面！先一睹taffy黑影之下的超美丽身姿吧？<br/> 6.6万  974<br/>视频<br/>永雏塔菲<br/><br/><br/>TOP3&nbsp;V子（三连+14748，传播 7分）<br/><br/>22:17<br/>从真・厉害到傻憨憨，就只有一步之遥《V子努力走红中！》#24<br/> 11.9万  497<br/>视频<br/>V子Official<br/><br/><br/>TOP4&nbsp;星宮汐（三连+6394，传播 6.5分）<br/><br/>03:00<br/>黑心汐酱当阿梓的老板<br/> 4.4万  144<br/>视频<br/>星宮汐Official<br/><br/><br/>TOP5&nbsp;珈乐Carol（三连+4843，传播 8.1分）<br/><br/>02:09<br/>【珈乐】皇珈骑士干得漂亮！！！【直播剪辑】<br/> 14.2万  3524<br/>视频<br/>珈乐Carol<br/><br/><br/>TOP6&nbsp;伊万_iiivan（三连+4780，传播 8.9分）<br/><br/>02:00<br/>女主播那里变大了 ？这不是很正常的事嘛！<br/> 23万  561<br/>视频<br/>伊万_iiivan<br/><br/><br/>TOP7&nbsp;小希小桃（三连+4078，传播 5分）<br/><br/>08:51<br/>三句话让阅卷老师多给我180分【沙雕观察4.14】<br/> 7.9万  369<br/>视频<br/>小希小桃Channel<br/><br/>TOP8 夏诺雅_shanoa（三连+4078，传播 4分）<br/><br/>04:28<br/>夏诺雅在你的耳边说出究极台词/しゃのあが君の耳元で究極なセリフを<br/> 1.7万  202<br/>视频<br/>夏诺雅_shanoa<br/><br/>TOP9&nbsp;花园Serena（三连+3947，传播 4.4分）<br/><br/>05:01<br/>【五分钟看猫猫】为了DD们，猫猫竟然放弃了……<br/> 3万  298<br/>视频<br/>花园Serena<br/><br/>TOP10 科尔（三连+3770，传播 7.7分）<br/><br/>00:10<br/>“主播能吐个舌头吗？”<br/> 27.8万  397<br/>视频<br/>科尔Channel<br/><br/><br/>每日虚拟UP主（VUP、VTuber）最佳音乐视频投稿TOP10<br/><br/><br/>TOP1&nbsp;乃琳、贝拉、向晚、嘉然（三连+18324，传播 4.8分）<br/><br/>02:51<br/>【向晚原创】水母之歌，你们就是那一束追光！（直播剪辑）<br/> 9.4万  2439<br/>视频<br/>向晚大魔王<br/><br/>TOP2&nbsp;珈乐Carol（三连+14867，传播 2.1分）<br/><br/>02:56<br/>【翻唱】《马马嘟嘟骑》【直播剪辑】<br/> 6.7万  2069<br/>视频<br/>珈乐Carol<br/><br/><br/>TOP3&nbsp;hanser、七海Nana7mi（三连+10499，传播 5分）<br/><br/>03:50<br/>(七海Xhanser) GETCHA！<br/> 18.9万  2118<br/>视频<br/>hanser<br/><br/><br/>TOP4&nbsp;勺Shaun（三连+10486，传播 4.7分）<br/><br/>03:47<br/>试卷上写下情书-《卷》<br/> 9.4万  499<br/>视频<br/>因你而在的梦<br/><br/>TOP5&nbsp;金克茜Jinxy（三连+3988，传播 5.3分）<br/><br/>01:13<br/>建议一定要听的女声版！《Dive Back In Time》【时光代理人OP】<br/> 8.8万  314<br/>视频<br/>金克茜Jinxy<br/><br/><br/>TOP6 绯赤艾莉欧（三连+3153，传播 0分）<br/><br/>04:45<br/>【翻唱】天城越え【飞越天城山】<br/> 6599  85<br/>视频<br/>绯赤艾莉欧Official<br/><br/><br/>TOP7&nbsp;hanser（三连+2733，传播 7.3分）<br/><br/>03:01<br/>凭啥咱不能过六一！（原创rap）<br/> 96.5万  3972<br/>视频<br/>hanser<br/><br/><br/>TOP8&nbsp;幸祜-koko-（三连+2332，传播 0分）<br/><br/>03:23<br/>幸祜 No.006「ASH」【Official Music Video】<br/> 8433  103<br/>视频<br/>幸祜-koko-<br/><br/><br/>TOP9&nbsp;艾因Eine、阿萨Aza（三连+2311，传播 6.9分）<br/><br/>04:18<br/>【涌潮悲歌】献给斯卡蒂的《海底》（凤凰传奇版/填词）【明日方舟】<br/> 182.7万  7199<br/>视频<br/>阿萨Aza<br/><br/>TOP10&nbsp;多多poi丶（三连+2287，传播 6.7分）<br/><br/>03:11<br/>【高考应援】《高kao战士》！突破瓶瓶瓶瓶颈，冲吖——【多多poi】<br/> 20.6万  1962<br/>视频<br/>多多poi丶<br/><br/>每日虚拟UP主（VUP、VTuber）最佳游戏视频投稿TOP10<br/><br/><br/>TOP1&nbsp;东堂比乃、绯赤艾莉欧（三连+3521，传播 4.1分）<br/><br/>08:00<br/>《pino不再是妹妹》<br/> 1.3万  69<br/>视频<br/>绯赤艾莉欧Official<br/><br/>TOP2 还有醒着的么（三连+3390，传播 6.3分）<br/><br/>09:25<br/>【摩尔庄园】说好的单纯快乐童年呢？！怎么会变成这样！<br/> 4万  179<br/>视频<br/>还有醒着的么<br/><br/><br/>TOP3&nbsp;小狼XF（三连+2871，传播 6.1分）<br/><br/>191:30<br/>【小狼XF】保加利亚狼王大战纸片人鬼《纸人》短视频+完整录播<br/> 1.7万  49<br/>视频<br/>小狼XF<br/><br/><br/>TOP4&nbsp;猫芒ベル（三连+2355，传播 5.4分）<br/><br/>04:39<br/>【艾希icey】游戏很好玩，但玩旁白更好玩<br/> 1.5万  94<br/>视频<br/>猫芒ベル_Official<br/><br/><br/>TOP5&nbsp;赫卡Tia（三连+2346，传播 6.3分）<br/><br/>03:59<br/>我们喜欢玩萝莉都是策划的错<br/> 1.5万  109<br/>视频<br/>赫卡Tia<br/><br/><br/>TOP6 璐璐咔小猪头（三连+2012，传播 8分）<br/><br/>51:34<br/>【璐璐咔】原神海岛全隐藏任务/成就合集，错过了就没有了哦<br/> 4.9万  84<br/>视频<br/>璐璐咔小猪头<br/><br/><br/>TOP7&nbsp;水卜泡泡（三连+1876，传播 8.8分）<br/><br/>01:53<br/>探路者必学的回收钩教程,别再傻傻地用中心点出钩了!!<br/> 8.8万  219<br/>视频<br/>水卜泡泡<br/><br/>TOP8&nbsp;玉白（三连+1708，传播 9分）<br/><br/>10:06<br/>【玉白】日本vup在APEX里假装不会中文可怜队友！（直播切片）<br/> 66.4万  1247<br/>视频<br/>玉白_official<br/><br/><br/>TOP9&nbsp;阿萨Aza（三连+1625，传播 2.1分）<br/><br/>2797:58<br/>阿萨的六月录播合集！<br/> 4994  49<br/>视频<br/>阿萨Aza<br/><br/><br/>TOP10&nbsp;AIChannel中国绊爱（三连+1601，传播 6.4分）<br/><br/>30:27<br/>【白色情人节】一不小心把女主给烧了？<br/> 2.5万  190<br/>视频<br/>AIChannel中国绊爱<br/><br/><br/><br/>更多精彩，每天都有！<br/><br/> 作者：V兔波 https://www.bilibili.com/read/cv11683964?from=category_2 出处：bilibili</font></b></p>', 'https://gulimaill-hello-test.oss-cn-beijing.aliyuncs.com/quick_bbs/image/20210611/85dcaf1f98e0d405bb06ef12882a7e19bb257382.jpg@1320w_934h.webp', 0, 0, 0, 0, '2021-06-12 01:27:53', '2021-06-12 01:27:53');
INSERT INTO `bbs_article` VALUES (42, 5, 1, '原作党表示你做不好就别做嘛，奇怪的故事线，奇怪的省略和魔改，奇怪的分镜和3D，奇怪的人类ᕕ( ᐛ )ᕗ', '原著里蜘蛛子大战魔王以及魔族加人类大战精灵加勇者都是高潮，我之前看他的双线程的时间安排，感觉是想把这两部分高潮放在一集一起热，一直在等这两个高潮，结果今天看完这不温不火的，以及劣质的制作，忍无可忍了。。。', '大蜘蛛', '<p style=\"text-align:center;\"><b><font color=\"#c24f4a\">原作党表示你做不好就别做嘛，奇怪的故事线，奇怪的省略和魔改，奇怪的分镜和3D，奇怪的人类ᕕ( ᐛ )ᕗ<br/><br/>原著里蜘蛛子大战魔王以及魔族加人类大战精灵加勇者都是高潮，我之前看他的双线程的时间安排，感觉是想把这两部分高潮放在一集一起热，一直在等这两个高潮，结果今天看完这不温不火的，以及劣质的制作，忍无可忍了。。。<br/><br/>制作组对不起，你们这制作着实劝退。前期这么好的节奏能给你玩成这样，整这活够烂，取消追番了，再见！！！&nbsp;</font></b></p><p style=\"text-align:center;\"><img src=\"https://gulimaill-hello-test.oss-cn-beijing.aliyuncs.com/quick_bbs/image/20210611/a930t-iz4jy.png\" data-href=\"%23\" style=\"max-width:100%;\" contenteditable=\"false\"/></p>', 'https://gulimaill-hello-test.oss-cn-beijing.aliyuncs.com/quick_bbs/image/20210611/f9ce688adf467ec9764d840ad72d31e6133eb5be.jpg@1320w_1854h.webp', 0, 0, 0, 0, '2021-06-12 01:29:49', '2021-06-12 01:29:49');
INSERT INTO `bbs_article` VALUES (43, 5, 1, '国漫《眷思量》开播，画风精致且美如画，为何关注度这么低？', '近几年国产动漫发大力，简直厉害得不行。举个简单的例子，斗罗大陆现在播放时长相比国产电视剧并不算多，但是播放量却已经达到了200亿。一些新起的动漫播放量更是惊人，除了若鸿文化短的可怕，明显是蹭播放量的以外，大部分动漫播放量都已经上亿。腾讯更是豪砸20多亿投资动漫产业，66部国漫即将上新。 作者：鹿之漫漫', '国漫', '<p style=\"line-height:2; text-align:left;\"><font color=\"#c24f4a\" face=\"黑体\" size=\"4\"><b style=\"\">近几年国产动漫发大力，简直厉害得不行。举个简单的例子，斗罗大陆现在播放时长相比国产电视剧并不算多，但是播放量却已经达到了200亿。一些新起的动漫播放量更是惊人，除了若鸿文化短的可怕，明显是蹭播放量的以外，大部分动漫播放量都已经上亿。腾讯更是豪砸20多亿投资动漫产业，66部国漫即将上新。</b></font></p><p style=\"line-height:2; text-align:center;\"><font color=\"#c24f4a\" face=\"黑体\" size=\"4\"><b style=\"\">&nbsp;</b></font><img src=\"https://gulimaill-hello-test.oss-cn-beijing.aliyuncs.com/quick_bbs/image/20210614/e970070d4f2ec64fbe0eb111c2fe4acd495d9608.jpg@628w_838h_progressive.png\" data-href=\"%23\" contenteditable=\"false\" style=\"max-width: 100%;\"/><br/></p><p>而今天刚开播的国漫《眷思量》，在技术方面无可挑剔，建模算是国创最顶尖之一了，动作不僵硬，打斗戏不错，角色颜值都很高。不过，这部国漫的关注度为什么这么低呢？一起来看看吧！</p><p style=\"text-align:center;\"><img src=\"https://gulimaill-hello-test.oss-cn-beijing.aliyuncs.com/quick_bbs/image/20210614/cc30076b024e313baae4a315312d8c9672eead5c.jpg@540w_720h_progressive.png\" data-href=\"%23\" style=\"max-width:100%;\" contenteditable=\"false\"/><br/><h1 id=\"cqi1e\">第二：题材限制</h1><font face=\"标楷体\" style=\"background-color: rgb(194, 79, 74);\"><br/></font></p><p style=\"text-align:center;\"><font face=\"标楷体\" style=\"background-color: rgb(255, 255, 255);\" color=\"#c24f4a\" size=\"5\">《眷思量》属于是仙凡恋情的唯美爱情，这是属于偏向女性化的题材，而且这类题材的受众群体比较固定，人气不如《斗罗大陆》《斗破苍穹》之类的快节奏爽文型玄幻动画，确实很难成为爆款。如果制作方再因为外力刻意拖慢剧情，或是在人物性格转变上操之过急，这都很容易就会导致整体质量雪崩，甚至会招来一片骂声。</font></p><p style=\"text-align:center;\"><img src=\"https://gulimaill-hello-test.oss-cn-beijing.aliyuncs.com/quick_bbs/image/20210614/94312f9c0363e2e0e45d36f1d4a14ad877edfaa5.jpg@600w_800h_progressive.png\" data-href=\"%23\" style=\"max-width:100%;\" contenteditable=\"false\"/></p>', 'https://gulimaill-hello-test.oss-cn-beijing.aliyuncs.com/quick_bbs/image/20210614/94312f9c0363e2e0e45d36f1d4a14ad877edfaa5.jpg@600w_800h_progressive.png', 0, 0, 0, 0, '2021-06-14 20:00:54', '2021-06-14 20:00:54');
INSERT INTO `bbs_article` VALUES (44, 5, 1, '测试', '测发到付', '啊啊', '<p><img src=\"https://gulimaill-hello-test.oss-cn-beijing.aliyuncs.com/quick_bbs/image/20210620/XW2Z($3V75)0XZFJX1YWB7N.png\" data-href=\"%23\" style=\"max-width:100%;\" contenteditable=\"false\"/></p>', 'https://gulimaill-hello-test.oss-cn-beijing.aliyuncs.com/quick_bbs/image/20210620/ff52076201e226beb16def4af8a88d2399b84bea.jpg@1200w_1680h.webp', 0, 0, 0, 0, '2021-06-20 15:14:01', '2021-06-20 15:14:01');
INSERT INTO `bbs_article` VALUES (45, 7, 1, '​泳池', '​泳池', '​泳池', '<p data-we-empty-p=\"\" style=\"text-align:center;\"><h1><b><font color=\"#c24f4a\">泳池</font></b></h1><img src=\"https://gulimaill-hello-test.oss-cn-beijing.aliyuncs.com/quick_bbs/image/20210620/04a332d647418cc25eba69712db139a19d345248.jpg\" data-href=\"%23\" style=\"max-width:100%;\" contenteditable=\"false\"/><br/></p>', 'https://gulimaill-hello-test.oss-cn-beijing.aliyuncs.com/quick_bbs/image/20210620/0ac0e8da6ff16868edcdfd30a7aaf00d8a7895f4.jpg', 0, 0, 0, 0, '2021-06-20 16:27:44', '2021-06-20 16:27:44');
INSERT INTO `bbs_article` VALUES (46, 7, 1, '动漫美图（第七期）', '嗨，大家好，我是美图专栏一个小小的萌新up主：我对爱酱一心一意', '美图', '<p style=\"text-align:center;\"><b><font size=\"5\" color=\"#f9963b\"><font face=\"黑体\">嗨，大家好，我是美图专栏一个小小的萌新up主：我对爱酱一心一意！（今天是第七次为大家带来动漫美图，今天是绅士图，希望大家喜欢。）<br/><br/>喜欢的各位可以点赞，收藏，投币，特别喜欢的可以关注一下我。（作品最终所有权归作者所有，我只是个小小的搬运工，如有侵权可以通知我删除）（弱弱说</font>一句可以告诉我你们想看什么，评论区写下来吧）</font></b></p><p style=\"text-align:center;\"><img src=\"https://gulimaill-hello-test.oss-cn-beijing.aliyuncs.com/quick_bbs/image/20210620/57b562e623df663b61e2b5d7285d0a3553db1e0c.jpg\" data-href=\"%23\" style=\"max-width:100%;\" contenteditable=\"false\"/><img src=\"https://gulimaill-hello-test.oss-cn-beijing.aliyuncs.com/quick_bbs/image/20210620/2917e66fe2c467e0ac131ea8eb38fe55c9be7e90.jpg\" data-href=\"%23\" style=\"max-width:100%;\" contenteditable=\"false\"/><img src=\"https://gulimaill-hello-test.oss-cn-beijing.aliyuncs.com/quick_bbs/image/20210620/06505e97df99c4814273f91920268b62637c5de0.jpg\" data-href=\"%23\" style=\"max-width:100%;\" contenteditable=\"false\"/><img src=\"https://gulimaill-hello-test.oss-cn-beijing.aliyuncs.com/quick_bbs/image/20210620/07895c2c30772aa2be5bd41944d2e724ca5786c8.jpg\" data-href=\"%23\" style=\"max-width:100%;\" contenteditable=\"false\"/><img src=\"https://gulimaill-hello-test.oss-cn-beijing.aliyuncs.com/quick_bbs/image/20210620/76769938c7de67ddeeefea58aa2288d81fb174f4.jpg\" data-href=\"%23\" style=\"max-width:100%;\" contenteditable=\"false\"/><img src=\"https://gulimaill-hello-test.oss-cn-beijing.aliyuncs.com/quick_bbs/image/20210620/aea30b36be8028b0dbc32506cd3e6d89d6aecd98.jpg\" data-href=\"%23\" style=\"max-width:100%;\" contenteditable=\"false\"/><img src=\"https://gulimaill-hello-test.oss-cn-beijing.aliyuncs.com/quick_bbs/image/20210620/bf035b4c9d9a52f181169ecc49a6f5a2ba346e63.jpg\" data-href=\"%23\" style=\"max-width:100%;\" contenteditable=\"false\"/><img src=\"https://gulimaill-hello-test.oss-cn-beijing.aliyuncs.com/quick_bbs/image/20210620/dd65cc4cdbeb2d7fc2e9c0a933ac93faad255da1.jpg\" data-href=\"%23\" style=\"max-width:100%;\" contenteditable=\"false\"/><img src=\"https://gulimaill-hello-test.oss-cn-beijing.aliyuncs.com/quick_bbs/image/20210620/e5354422a51e125a5646d01762aaadad10497124.jpg\" data-href=\"%23\" style=\"max-width:100%;\" contenteditable=\"false\"/></p><p><br/></p>', 'https://gulimaill-hello-test.oss-cn-beijing.aliyuncs.com/quick_bbs/image/20210620/d76e476b6f36b395c0be5b4819758f5209e15c27.jpg', 0, 0, 0, 0, '2021-06-20 19:34:11', '2021-06-20 19:34:11');

-- ----------------------------
-- Table structure for bbs_categoriza
-- ----------------------------
DROP TABLE IF EXISTS `bbs_categoriza`;
CREATE TABLE `bbs_categoriza`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '分类id',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '分类名称',
  `sort` int(11) NOT NULL DEFAULT 0 COMMENT '排序',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `logo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图标logo',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bbs_categoriza
-- ----------------------------
INSERT INTO `bbs_categoriza` VALUES (1, '生活', 0, '2021-06-03 11:46:20', '2021-06-03 11:46:23', 'el-icon-lollipop');
INSERT INTO `bbs_categoriza` VALUES (2, '休闲', 0, '2021-06-03 11:46:31', '2021-06-23 11:46:34', 'el-icon-lollipop');
INSERT INTO `bbs_categoriza` VALUES (3, '美食', 0, '2021-06-25 11:46:50', '2021-07-04 11:46:54', 'el-icon-lollipop');
INSERT INTO `bbs_categoriza` VALUES (4, '闲聊', 0, '2021-06-27 11:47:05', '2021-06-30 11:47:09', 'el-icon-lollipop');

-- ----------------------------
-- Table structure for bbs_comment
-- ----------------------------
DROP TABLE IF EXISTS `bbs_comment`;
CREATE TABLE `bbs_comment`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '评论id',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '评论内容',
  `parent_id` int(11) NOT NULL DEFAULT 0 COMMENT '父id',
  `likes` int(11) NULL DEFAULT NULL COMMENT '点赞数',
  `sort` int(11) NOT NULL DEFAULT 0 COMMENT '排序',
  `u_id` int(11) NULL DEFAULT NULL COMMENT '评论用户id',
  `a_id` int(11) NULL DEFAULT NULL COMMENT '评论文章id',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '评论时间',
  `replay_id` int(11) NULL DEFAULT NULL COMMENT '回复用户id',
  `replay_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '回复的用户名(一级默认为空)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 142 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bbs_comment
-- ----------------------------
INSERT INTO `bbs_comment` VALUES (1, '这个小姐姐非常可爱', 0, 100, 0, 1, 38, '2021-06-10 15:53:37', '2021-06-10 15:53:40', 0, NULL);
INSERT INTO `bbs_comment` VALUES (2, '你好恶心', 1, 10, 0, 5, 38, '2021-06-10 16:09:45', '2021-06-10 16:09:47', 1, 'islandwell');
INSERT INTO `bbs_comment` VALUES (3, '你好恶心', 1, 10, 0, 5, 38, '2021-06-10 16:09:45', '2021-06-10 16:09:47', 1, 'islandwell');
INSERT INTO `bbs_comment` VALUES (4, '你好恶心', 1, 10, 0, 5, 38, '2021-06-10 16:09:45', '2021-06-10 16:09:47', 1, 'islandwell');
INSERT INTO `bbs_comment` VALUES (5, '你好恶心', 1, 10, 0, 5, 38, '2021-06-10 16:09:45', '2021-06-10 16:09:47', 1, 'islandwell');
INSERT INTO `bbs_comment` VALUES (6, '你好恶心', 1, 10, 0, 5, 38, '2021-06-10 16:09:45', '2021-06-10 16:09:47', 1, 'islandwell');
INSERT INTO `bbs_comment` VALUES (7, '你好恶心', 1, 10, 0, 1, 38, '2021-06-10 16:09:45', '2021-06-10 16:09:47', 1, 'islandwell');
INSERT INTO `bbs_comment` VALUES (8, '你好恶心', 1, 10, 0, 5, 38, '2021-06-10 16:09:45', '2021-06-10 16:09:47', 1, 'islandwell');
INSERT INTO `bbs_comment` VALUES (9, '你好恶心', 1, 10, 0, 5, 38, '2021-06-10 16:09:45', '2021-06-10 16:09:47', 1, 'islandwell');
INSERT INTO `bbs_comment` VALUES (10, '你好恶心', 1, 10, 0, 5, 38, '2021-06-10 16:09:45', '2021-06-10 16:09:47', 1, 'islandwell');
INSERT INTO `bbs_comment` VALUES (11, '很好看', 0, 10, 0, 5, 38, '2021-06-10 16:09:45', '2021-06-10 16:09:47', 1, 'islandwell');
INSERT INTO `bbs_comment` VALUES (16, '很好看', 0, 10, 0, 1, 38, '2021-06-10 16:09:45', '2021-06-10 16:09:47', 1, 'islandwell');
INSERT INTO `bbs_comment` VALUES (18, '很好看', 0, 10, 0, 1, 38, '2021-06-10 16:09:45', '2021-06-10 16:09:47', 1, 'islandwell');
INSERT INTO `bbs_comment` VALUES (19, '很好看', 0, 10, 0, 5, 38, '2021-06-10 16:09:45', '2021-06-10 16:09:47', 1, 'islandwell');
INSERT INTO `bbs_comment` VALUES (20, '很好看', 0, 10, 0, 5, 38, '2021-06-10 16:09:45', '2021-06-10 16:09:47', 1, 'islandwell');
INSERT INTO `bbs_comment` VALUES (23, '非常好的一个文章', 0, 0, 0, 1, 37, '2021-06-10 20:47:32', '2021-06-10 20:47:32', 0, '');
INSERT INTO `bbs_comment` VALUES (25, '你是大傻逼吗', 0, 0, 0, 1, 38, '2021-06-10 20:57:28', '2021-06-10 20:57:28', 0, '');
INSERT INTO `bbs_comment` VALUES (26, '王毅松，王毅松', 0, 0, 0, 1, 38, '2021-06-10 20:58:25', '2021-06-10 20:58:25', 0, '');
INSERT INTO `bbs_comment` VALUES (27, '一起来游泳吧', 0, 0, 0, 1, 37, '2021-06-10 21:00:06', '2021-06-10 21:00:06', 0, '');
INSERT INTO `bbs_comment` VALUES (28, '这个就是印度？', 0, 0, 0, 1, 35, '2021-06-10 21:15:15', '2021-06-10 21:15:15', 0, '');
INSERT INTO `bbs_comment` VALUES (30, '1111', 0, 0, 0, 5, 34, '2021-06-10 21:20:03', '2021-06-10 21:20:03', 0, '');
INSERT INTO `bbs_comment` VALUES (34, '太帅了', 0, 0, 0, 1, 38, '2021-06-11 16:05:20', '2021-06-11 16:05:20', 0, '');
INSERT INTO `bbs_comment` VALUES (35, '爱了', 0, 0, 0, 1, 38, '2021-06-11 16:06:41', '2021-06-11 16:06:41', 0, '');
INSERT INTO `bbs_comment` VALUES (36, '不会吧', 0, 0, 0, 1, 38, '2021-06-11 16:07:02', '2021-06-11 16:07:02', 0, '');
INSERT INTO `bbs_comment` VALUES (37, '你好', 0, 0, 0, 1, 38, '2021-06-11 16:08:10', '2021-06-11 16:08:10', 0, '');
INSERT INTO `bbs_comment` VALUES (38, '阿哲', 0, 0, 0, 1, 38, '2021-06-11 16:10:40', '2021-06-11 16:10:40', 0, '');
INSERT INTO `bbs_comment` VALUES (41, '啊啊啊啊', 0, 0, 0, 1, 38, '2021-06-11 16:11:03', '2021-06-11 16:11:03', 0, '');
INSERT INTO `bbs_comment` VALUES (42, '王毅松', 0, 0, 0, 1, 38, '2021-06-11 16:11:28', '2021-06-11 16:11:28', 0, '');
INSERT INTO `bbs_comment` VALUES (45, '确实，你也是这么认为的吗', 0, 0, 0, 5, 37, '2021-06-11 16:30:04', '2021-06-11 16:30:04', 0, '');
INSERT INTO `bbs_comment` VALUES (46, '楼上的是真理', 0, 0, 0, 5, 37, '2021-06-11 16:31:41', '2021-06-11 16:31:41', 0, '');
INSERT INTO `bbs_comment` VALUES (48, '帅气的姐姐', 0, 0, 0, 5, 38, '2021-06-11 16:40:35', '2021-06-11 16:40:35', 0, '');
INSERT INTO `bbs_comment` VALUES (50, '印度人民在磨难当中，太难受了', 0, 0, 0, 5, 34, '2021-06-11 23:44:44', '2021-06-11 23:44:44', 0, '');
INSERT INTO `bbs_comment` VALUES (52, '眷思量真的挺好看的', 0, 0, 0, NULL, 43, '2021-06-14 22:38:39', '2021-06-14 22:38:39', 0, '');
INSERT INTO `bbs_comment` VALUES (53, '眷思量真的很好看呀', 0, 0, 0, 5, 43, '2021-06-14 22:39:26', '2021-06-14 22:39:26', 0, '');
INSERT INTO `bbs_comment` VALUES (55, '1', 0, 0, 0, 5, 43, '2021-06-19 13:31:17', '2021-06-19 13:31:17', 0, '');
INSERT INTO `bbs_comment` VALUES (56, '1', 0, 0, 0, 5, 43, '2021-06-19 13:31:19', '2021-06-19 13:31:19', 0, '');
INSERT INTO `bbs_comment` VALUES (57, '1', 0, 0, 0, 5, 43, '2021-06-19 13:31:21', '2021-06-19 13:31:21', 0, '');
INSERT INTO `bbs_comment` VALUES (58, '1', 0, 0, 0, 5, 43, '2021-06-19 13:31:22', '2021-06-19 13:31:22', 0, '');
INSERT INTO `bbs_comment` VALUES (59, '1', 0, 0, 0, 5, 43, '2021-06-19 13:31:23', '2021-06-19 13:31:23', 0, '');
INSERT INTO `bbs_comment` VALUES (60, '1', 0, 0, 0, 5, 43, '2021-06-19 13:31:23', '2021-06-19 13:31:23', 0, '');
INSERT INTO `bbs_comment` VALUES (61, '1', 0, 0, 0, 5, 43, '2021-06-19 13:31:25', '2021-06-19 13:31:25', 0, '');
INSERT INTO `bbs_comment` VALUES (62, '1', 0, 0, 0, 5, 43, '2021-06-19 13:31:25', '2021-06-19 13:31:25', 0, '');
INSERT INTO `bbs_comment` VALUES (63, '1', 0, 0, 0, 5, 43, '2021-06-19 13:31:25', '2021-06-19 13:31:25', 0, '');
INSERT INTO `bbs_comment` VALUES (64, '1', 0, 0, 0, 5, 43, '2021-06-19 13:31:28', '2021-06-19 13:31:28', 0, '');
INSERT INTO `bbs_comment` VALUES (65, '1', 0, 0, 0, 5, 43, '2021-06-19 13:31:30', '2021-06-19 13:31:30', 0, '');
INSERT INTO `bbs_comment` VALUES (66, '1', 0, 0, 0, 5, 43, '2021-06-19 13:31:31', '2021-06-19 13:31:31', 0, '');
INSERT INTO `bbs_comment` VALUES (67, '1', 0, 0, 0, 5, 43, '2021-06-19 13:31:31', '2021-06-19 13:31:31', 0, '');
INSERT INTO `bbs_comment` VALUES (68, '1', 0, 0, 0, 5, 43, '2021-06-19 13:31:31', '2021-06-19 13:31:31', 0, '');
INSERT INTO `bbs_comment` VALUES (69, '1', 0, 0, 0, 5, 43, '2021-06-19 13:31:32', '2021-06-19 13:31:32', 0, '');
INSERT INTO `bbs_comment` VALUES (70, '1', 0, 0, 0, 5, 43, '2021-06-19 13:31:32', '2021-06-19 13:31:32', 0, '');
INSERT INTO `bbs_comment` VALUES (71, '1', 0, 0, 0, 5, 43, '2021-06-19 13:31:32', '2021-06-19 13:31:32', 0, '');
INSERT INTO `bbs_comment` VALUES (72, '1', 0, 0, 0, 5, 43, '2021-06-19 13:31:33', '2021-06-19 13:31:33', 0, '');
INSERT INTO `bbs_comment` VALUES (73, '1', 0, 0, 0, 5, 43, '2021-06-19 13:31:33', '2021-06-19 13:31:33', 0, '');
INSERT INTO `bbs_comment` VALUES (74, '1', 0, 0, 0, 5, 43, '2021-06-19 13:31:34', '2021-06-19 13:31:34', 0, '');
INSERT INTO `bbs_comment` VALUES (75, '1', 0, 0, 0, 5, 43, '2021-06-19 13:31:34', '2021-06-19 13:31:34', 0, '');
INSERT INTO `bbs_comment` VALUES (76, '1', 0, 0, 0, 5, 43, '2021-06-19 13:31:35', '2021-06-19 13:31:35', 0, '');
INSERT INTO `bbs_comment` VALUES (77, '1', 0, 0, 0, 5, 43, '2021-06-19 13:31:35', '2021-06-19 13:31:35', 0, '');
INSERT INTO `bbs_comment` VALUES (78, '1', 0, 0, 0, 5, 43, '2021-06-19 13:31:36', '2021-06-19 13:31:36', 0, '');
INSERT INTO `bbs_comment` VALUES (79, '1', 0, 0, 0, 5, 43, '2021-06-19 13:31:36', '2021-06-19 13:31:36', 0, '');
INSERT INTO `bbs_comment` VALUES (80, '1', 0, 0, 0, 5, 43, '2021-06-19 13:31:36', '2021-06-19 13:31:36', 0, '');
INSERT INTO `bbs_comment` VALUES (81, '1', 0, 0, 0, 5, 43, '2021-06-19 13:31:38', '2021-06-19 13:31:38', 0, '');
INSERT INTO `bbs_comment` VALUES (82, '1', 0, 0, 0, 5, 43, '2021-06-19 13:31:38', '2021-06-19 13:31:38', 0, '');
INSERT INTO `bbs_comment` VALUES (83, '1', 0, 0, 0, 5, 43, '2021-06-19 13:31:39', '2021-06-19 13:31:39', 0, '');
INSERT INTO `bbs_comment` VALUES (84, '1', 0, 0, 0, 5, 43, '2021-06-19 13:31:39', '2021-06-19 13:31:39', 0, '');
INSERT INTO `bbs_comment` VALUES (85, '1', 0, 0, 0, 5, 43, '2021-06-19 13:31:39', '2021-06-19 13:31:39', 0, '');
INSERT INTO `bbs_comment` VALUES (86, '1', 0, 0, 0, 5, 43, '2021-06-19 13:31:40', '2021-06-19 13:31:40', 0, '');
INSERT INTO `bbs_comment` VALUES (87, '1', 0, 0, 0, 5, 43, '2021-06-19 13:31:40', '2021-06-19 13:31:40', 0, '');
INSERT INTO `bbs_comment` VALUES (88, '1', 0, 0, 0, 5, 43, '2021-06-19 13:31:41', '2021-06-19 13:31:41', 0, '');
INSERT INTO `bbs_comment` VALUES (89, '1', 0, 0, 0, 5, 43, '2021-06-19 13:31:41', '2021-06-19 13:31:41', 0, '');
INSERT INTO `bbs_comment` VALUES (90, '1', 0, 0, 0, 5, 43, '2021-06-19 13:31:41', '2021-06-19 13:31:41', 0, '');
INSERT INTO `bbs_comment` VALUES (91, '1', 0, 0, 0, 5, 43, '2021-06-19 13:31:42', '2021-06-19 13:31:42', 0, '');
INSERT INTO `bbs_comment` VALUES (92, '1', 0, 0, 0, 5, 43, '2021-06-19 13:31:42', '2021-06-19 13:31:42', 0, '');
INSERT INTO `bbs_comment` VALUES (93, '1', 0, 0, 0, 5, 43, '2021-06-19 13:31:43', '2021-06-19 13:31:43', 0, '');
INSERT INTO `bbs_comment` VALUES (94, '好的', 0, 0, 0, 5, 43, '2021-06-19 13:31:47', '2021-06-19 13:31:47', 0, '');
INSERT INTO `bbs_comment` VALUES (95, '1', 0, 0, 0, NULL, 43, '2021-06-19 13:58:09', '2021-06-19 13:58:09', 0, '');
INSERT INTO `bbs_comment` VALUES (96, '1', 0, 0, 0, 7, 45, '2021-06-20 19:30:11', '2021-06-20 19:30:11', 0, '');
INSERT INTO `bbs_comment` VALUES (97, '感谢分享', 0, 0, 0, 7, 46, '2021-06-20 19:35:32', '2021-06-20 19:35:32', 0, '');
INSERT INTO `bbs_comment` VALUES (98, '博主太好了', 0, 0, 0, 5, 46, '2021-06-20 19:36:19', '2021-06-20 19:36:19', 0, '');
INSERT INTO `bbs_comment` VALUES (99, '嘿嘿', 0, 0, 0, 5, 46, '2021-06-20 19:41:52', '2021-06-20 19:41:52', 0, '');
INSERT INTO `bbs_comment` VALUES (100, '1', 0, 0, 0, 5, 46, '2021-06-20 19:42:10', '2021-06-20 19:42:10', 0, '');
INSERT INTO `bbs_comment` VALUES (101, '1', 0, 0, 0, 5, 46, '2021-06-20 19:42:11', '2021-06-20 19:42:11', 0, '');
INSERT INTO `bbs_comment` VALUES (102, '1', 0, 0, 0, 5, 46, '2021-06-20 19:42:11', '2021-06-20 19:42:11', 0, '');
INSERT INTO `bbs_comment` VALUES (103, '1', 0, 0, 0, 5, 46, '2021-06-20 19:42:11', '2021-06-20 19:42:11', 0, '');
INSERT INTO `bbs_comment` VALUES (104, '1', 0, 0, 0, 5, 46, '2021-06-20 19:42:12', '2021-06-20 19:42:12', 0, '');
INSERT INTO `bbs_comment` VALUES (105, '1', 0, 0, 0, 5, 46, '2021-06-20 19:42:12', '2021-06-20 19:42:12', 0, '');
INSERT INTO `bbs_comment` VALUES (106, '1', 0, 0, 0, 5, 46, '2021-06-20 19:42:12', '2021-06-20 19:42:12', 0, '');
INSERT INTO `bbs_comment` VALUES (107, '1', 0, 0, 0, 5, 46, '2021-06-20 19:42:13', '2021-06-20 19:42:13', 0, '');
INSERT INTO `bbs_comment` VALUES (108, '1', 0, 0, 0, 5, 46, '2021-06-20 19:42:13', '2021-06-20 19:42:13', 0, '');
INSERT INTO `bbs_comment` VALUES (109, '1', 0, 0, 0, 5, 46, '2021-06-20 19:42:14', '2021-06-20 19:42:14', 0, '');
INSERT INTO `bbs_comment` VALUES (110, '1', 0, 0, 0, 5, 46, '2021-06-20 19:42:14', '2021-06-20 19:42:14', 0, '');
INSERT INTO `bbs_comment` VALUES (111, '1', 0, 0, 0, 5, 46, '2021-06-20 19:42:14', '2021-06-20 19:42:14', 0, '');
INSERT INTO `bbs_comment` VALUES (112, '1', 0, 0, 0, 5, 46, '2021-06-20 19:42:15', '2021-06-20 19:42:15', 0, '');
INSERT INTO `bbs_comment` VALUES (113, '1', 0, 0, 0, 5, 46, '2021-06-20 19:42:15', '2021-06-20 19:42:15', 0, '');
INSERT INTO `bbs_comment` VALUES (114, '1', 0, 0, 0, 5, 46, '2021-06-20 19:42:16', '2021-06-20 19:42:16', 0, '');
INSERT INTO `bbs_comment` VALUES (115, '1', 0, 0, 0, 5, 46, '2021-06-20 19:42:16', '2021-06-20 19:42:16', 0, '');
INSERT INTO `bbs_comment` VALUES (116, '1', 0, 0, 0, 5, 46, '2021-06-20 19:42:16', '2021-06-20 19:42:16', 0, '');
INSERT INTO `bbs_comment` VALUES (117, '1', 0, 0, 0, 5, 46, '2021-06-20 19:42:16', '2021-06-20 19:42:16', 0, '');
INSERT INTO `bbs_comment` VALUES (118, '1', 0, 0, 0, 5, 46, '2021-06-20 19:42:17', '2021-06-20 19:42:17', 0, '');
INSERT INTO `bbs_comment` VALUES (119, '1', 0, 0, 0, 5, 46, '2021-06-20 19:42:17', '2021-06-20 19:42:17', 0, '');
INSERT INTO `bbs_comment` VALUES (120, '1', 0, 0, 0, 5, 46, '2021-06-20 19:42:17', '2021-06-20 19:42:17', 0, '');
INSERT INTO `bbs_comment` VALUES (121, '1', 0, 0, 0, 5, 46, '2021-06-20 19:42:18', '2021-06-20 19:42:18', 0, '');
INSERT INTO `bbs_comment` VALUES (122, '1', 0, 0, 0, 5, 46, '2021-06-20 19:42:18', '2021-06-20 19:42:18', 0, '');
INSERT INTO `bbs_comment` VALUES (123, '1', 0, 0, 0, 5, 46, '2021-06-20 19:42:18', '2021-06-20 19:42:18', 0, '');
INSERT INTO `bbs_comment` VALUES (124, '1', 0, 0, 0, 5, 46, '2021-06-20 19:42:18', '2021-06-20 19:42:18', 0, '');
INSERT INTO `bbs_comment` VALUES (125, '1', 0, 0, 0, 5, 46, '2021-06-20 19:42:19', '2021-06-20 19:42:19', 0, '');
INSERT INTO `bbs_comment` VALUES (126, '1', 0, 0, 0, 5, 46, '2021-06-20 19:42:19', '2021-06-20 19:42:19', 0, '');
INSERT INTO `bbs_comment` VALUES (127, '1', 0, 0, 0, 5, 46, '2021-06-20 19:42:19', '2021-06-20 19:42:19', 0, '');
INSERT INTO `bbs_comment` VALUES (128, '1', 0, 0, 0, 5, 46, '2021-06-20 19:42:20', '2021-06-20 19:42:20', 0, '');
INSERT INTO `bbs_comment` VALUES (129, '1', 0, 0, 0, 5, 46, '2021-06-20 19:42:20', '2021-06-20 19:42:20', 0, '');
INSERT INTO `bbs_comment` VALUES (130, '1', 0, 0, 0, 5, 46, '2021-06-20 19:42:20', '2021-06-20 19:42:20', 0, '');
INSERT INTO `bbs_comment` VALUES (131, '1', 0, 0, 0, 5, 46, '2021-06-20 19:42:20', '2021-06-20 19:42:20', 0, '');
INSERT INTO `bbs_comment` VALUES (132, '1', 0, 0, 0, 5, 46, '2021-06-20 19:42:20', '2021-06-20 19:42:20', 0, '');
INSERT INTO `bbs_comment` VALUES (133, '1', 0, 0, 0, 5, 46, '2021-06-20 19:42:21', '2021-06-20 19:42:21', 0, '');
INSERT INTO `bbs_comment` VALUES (134, '1', 0, 0, 0, 5, 46, '2021-06-20 19:42:21', '2021-06-20 19:42:21', 0, '');
INSERT INTO `bbs_comment` VALUES (135, '1', 0, 0, 0, 5, 46, '2021-06-20 19:42:21', '2021-06-20 19:42:21', 0, '');
INSERT INTO `bbs_comment` VALUES (136, '1', 0, 0, 0, 5, 46, '2021-06-20 19:42:22', '2021-06-20 19:42:22', 0, '');
INSERT INTO `bbs_comment` VALUES (137, '嘿嘿', 0, 0, 0, 5, 46, '2021-06-20 19:42:22', '2021-06-20 19:42:22', 0, '');
INSERT INTO `bbs_comment` VALUES (138, '1', 0, 0, 0, 5, 46, '2021-06-20 19:42:22', '2021-06-20 19:42:22', 0, '');
INSERT INTO `bbs_comment` VALUES (139, '1', 0, 0, 0, 5, 46, '2021-06-20 19:42:22', '2021-06-20 19:42:22', 0, '');
INSERT INTO `bbs_comment` VALUES (140, '1', 0, 0, 0, 5, 46, '2021-06-20 19:42:23', '2021-06-20 19:42:23', 0, '');
INSERT INTO `bbs_comment` VALUES (141, '嘿嘿', 0, 0, 0, 5, 46, '2021-06-20 19:42:31', '2021-06-20 19:42:31', 0, '');

-- ----------------------------
-- Table structure for bbs_test
-- ----------------------------
DROP TABLE IF EXISTS `bbs_test`;
CREATE TABLE `bbs_test`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `password` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bbs_test
-- ----------------------------
INSERT INTO `bbs_test` VALUES (1, 'islandwell', '123456');

-- ----------------------------
-- Table structure for bbs_user
-- ----------------------------
DROP TABLE IF EXISTS `bbs_user`;
CREATE TABLE `bbs_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户名',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '头像',
  `profile` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '简介',
  `password` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '密码',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bbs_user
-- ----------------------------
INSERT INTO `bbs_user` VALUES (1, 'islandwell', 'https://gulimaill-hello-test.oss-cn-beijing.aliyuncs.com/quick_bbs/image/20210604/c4fbbd3aceed4302a1c88accfc1f076de83eb10a.png@292w_220h.webp', '这个是修改的介绍', '123456', '2021-06-02 23:10:15', '2021-06-19 11:38:42');
INSERT INTO `bbs_user` VALUES (5, 'admin', 'https://gulimaill-hello-test.oss-cn-beijing.aliyuncs.com/quick_bbs/image/20210620/1c1439e796025f850e399ebcc81cefc2.jpg', '风格刚刚', '123456', '2021-06-08 17:19:50', '2021-06-20 19:44:04');
INSERT INTO `bbs_user` VALUES (6, 'zyw', 'https://gulimaill-hello-test.oss-cn-beijing.aliyuncs.com/quick_bbs/image/20210608/ce6a515b9cdcaaff439c0047412d5b86.jpeg', '', '123456', '2021-06-10 21:25:55', '2021-06-10 21:25:55');
INSERT INTO `bbs_user` VALUES (7, '092218124', 'https://gulimaill-hello-test.oss-cn-beijing.aliyuncs.com/quick_bbs/image/20210620/20210620162202.jpg', '092218124朱耀威', 'jswxqq1999', '2021-06-20 16:21:11', '2021-06-20 16:23:23');

SET FOREIGN_KEY_CHECKS = 1;
