import { ref } from "vue"

export const loginForm = ref({
		email: '',
		passwd: ''
})
export const loginFormRules = ref({
	email: [
		{ required: true, message: '请输入邮箱', trigger: 'blur' }
	],
	passwd: [
		{ required: true, message: '请输入密码', trigger: 'blur' }
	],
})