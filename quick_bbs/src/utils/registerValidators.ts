import { ref } from "vue"

export	const registerForm = ref({
	name: '',
	email: '',
	passwd: '',
	passwd2: '',
	role: 'user'
})

const validatePass2 = (rule, value, callback) => {
	if (value === '') {
		callback(new Error('请再次输入密码'));
	} else if (value !== registerForm.value.passwd) {
		callback(new Error('两次输入密码不一致!'));
	} else {
		callback();
	}
}

export const registerFormRules = ref({
	name: [
		{ required: true, message: '请输入用户名', trigger: 'blur' }
	],
	email: [
		{ required: true, message: '请输入邮箱', trigger: 'blur' }
	],
	passwd: [
		{ required: true, message: '请输入密码', trigger: 'blur' }
	],
	passwd2: [
		{ required: true, message: '请重复密码', trigger: 'blur' },
		{ validator: validatePass2, message: '两次密码输入不一致', trigger: 'blur'}
	]
})