import Vue from 'vue'
import Vuex from 'vuex'

//挂载Vuex
Vue.use(Vuex)

//创建VueX对象
const store = new Vuex.Store({
    state:{
        //存放的键值对就是所要管理的状态
        name:'helloVueX',
        username:localStorage.getItem("username"),
        avatar:localStorage.getItem("avatar"),
        islogin:localStorage.getItem("islogin")
    },
    mutations:{
        setUsername(state,username){
            //写入缓存
            localStorage.setItem("username", username);
            state.username = state;
        },
        setAvatar(state,avatar){
            localStorage.setItem("avatar",avatar);
            state.avatar = state;
        },
        setIsLogin(state,islogin){
            localStorage.setItem("islogin",true);
            state.islogin = true;
        },
        loginOut(state){
            localStorage.removeItem("username");
            localStorage.removeItem("avatar");
            localStorage.removeItem("islogin");
            state.islogin = false;
        }
    }
})

export default store