import Vue from 'vue'
import Router from 'vue-router'
const index = () => import('@/views/index')
const register = () => import('@/views/register')
const login = () => import('@/views/login')
const editarticle = () => import('@/views/editarticle')
const article = () => import('@/views/article')

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/article/:id',
      name: 'article',
      component: article,
    },
    {
      path: '/',
      name: 'index',
      component: index
    },
    {
      path: '/register',
      name: 'register',
      component: register
    },
    {
      path: '/login',
      name: 'login',
      component: login
    },
    {
      path: '/editarticle',
      name: 'editarticle',
      component: editarticle
    }
  ]
})
