<?php
     class R{
        public $code;
        public $message;
        public $data;
        public static function ok($data = null){
            return R::toJson(new R(200,"成功",$data));
        }
        public static function error($code,$message){
            return R::toJson(new R($code,$message,null));
        }
        public static function toJson($obj){
            $res = array(
                "code" => $obj->code,
                "message" => $obj->message,
                "data" => $obj->data
            );
            return json_encode($res);
        }
        # R构造函数
        public function __construct($code=200, $message="成功", $data=null){
            $this -> code  = $code;
            $this -> message   = $message;
            $this -> data = $data;
        }
        
     }
      
?>