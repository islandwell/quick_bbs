<?php
    # 数据库对象
    # 单例模式   
    class Db{
        static private $DB_CONFIG=[
            "host" => "localhost",
            "username" => "root",
            "password" => "1111",
            "database" => "quick_bbs"
        ];
        private $name;
        private function __construct(){}
        static public $connect;
        static public function getinstance(){
            ## 检查connect是否已经连接，没有连接数据库连接数据库
            $DB_CONFIG = Db::$DB_CONFIG;
            if(!self::$connect){
                # 连接数据库
                $host = $DB_CONFIG['host'];
                $database = $DB_CONFIG['database'];
                $dbh = new PDO("mysql:host=$host;dbname=$database",$DB_CONFIG['username'],$DB_CONFIG['password']);
                self::$connect = $dbh;
            } 
            return self::$connect;
        }
    }
?>