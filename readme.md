# 超级轻社区 
> 简介：
> 目前该测试版已经部署在了http://bbs.zhuyaowei.cn  当中
> 
> 该项目是php课程设计的大作业，目前功能不是很完善，望大家谅解！
>
> 目前该轻社区还属于开发状态，版本号V1.0 Beta - a1
>
> 这是一个极简的轻社区bbs，前端vue + axios+ vuex+vue-router 后端php
>
> 前端的富文本编辑器使用的是wangeditor
>
> V1.0 Beta - a1完成的功能如下：
>
> 用户的登录、用户信息、注册、发布文章、查看分类、查看文章列表、评论的查看和评论、上传文件 阿里oss 和 删除功能
>
> 目前这个版本只是这个版本的第二次发布，功能还不是很完全，后续会一步一步的完善该社区，还在维护当中。

社区的主页如下：

![社区主页](md-source/image-20210612005349738.png)

登录界面如下：

![登录](md-source/image-20210612010015914.png)

用户的注册

![用户注册](md-source/image-20210612010227163.png)

文章界面

![image-20210612010522830](md-source/image-20210612010522830.png)

评论列表

![评论列表](md-source/image-20210612010724982.png)

发布列表

![image-20210612012933609](md-source/image-20210612012933609.png)

主页

![image-20210612013318745](md-source/image-20210612013318745.png)

![image-20210612013330264](md-source/image-20210612013330264.png)

## 下个测试版本任务 Beta - b1

1. 修复登录session时间太短导致用户登陆失效问题
2. 加入文章点赞和评论点赞功能
3. 加入统计浏览数功能
4. 主页空出的3加入文章热度统计
5. 评论和文章内容过滤

## 运行的方法如下

![image-20210612014149533](md-source/image-20210612014149533.png)

>resource资源文件
>
>~~source\font字体~~
>
>test测试脚本
>
>util 封装的工具类
>
>vendor 阿里oss依赖的插件

## 前端

### 1.前端部分是quick_bbs模块

```shell
npm install
```

### 2.修改index.js当中的target为自己部署的域名或者ip

![image-20210612014801463](md-source/image-20210612014801463.png)

3. 使用npm运行

   ```shell
   npm run dev
   ```

## 后端php

### 文章更新当中

